import math


while True:

    fx_lst = [[], [], []]

    try:
        data = list(map(float, input('Введите минимум, максимум, а, размер шагов через пробел: ').split()))
        x_min = float(data[0])
        x_max = float(data[1])
        a = float(data[2])
        step = float(data[3])
    except ValueError:
        print('Неверный ввод')
        exit(1)

    while x_min < x_max:
        try:
            g = (- 2 * (7 * a ** 2 - 17 * a * x_min + 6 * x_min ** 2)) / (21 * a ** 2 + a * x_min - 36 * x_min ** 2)
            fx_lst[0].append(g)
        except ZeroDivisionError:
            fx_lst[0].append(None)
        try:
            f = (math.sin(math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))) / (math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))
            fx_lst[1].append(f)
        except ZeroDivisionError:
            fx_lst[1].append(None)
        try:
            y = (-math.atanh(8 * a ** 2 - 27 * a * x_min - 20 * x_min ** 2))
            fx_lst[2].append(y)
        except ValueError:
            fx_lst[2].append(None)
        x_min += step

    for i in range(len(fx_lst[0])):
        print(f'g = {fx_lst[0][i]}\t y = {fx_lst[1][i]}\t f = {fx_lst[2][i]}')

    check = input('Вы хотите выйти? Введите yes или no: ')
    if check == 'yes':
        break
    elif check == 'no':
        pass