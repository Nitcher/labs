import time
import matplotlib.pyplot as plt
import random


def point_check(point, center, rad):
    return (center[0] - point[0]) ** 2 + (center[1] - point[1]) ** 2 <= rad ** 2


def count_points(points, center, radius):
    output = 0
    for point in points:
        if point_check(point, center, radius):
            output += 1
    return output


n_min = 10e4
n_max = 10e5
n_step = 10e4

n_data = []
time_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    points = [(random.uniform(-10, 10), random.uniform(-10, 10)) for i in range(n)]
    center = (random.uniform(-10, 10), random.uniform(-10, 10))
    rad = n / 4
    for _ in range(3):
        start = time.time()
        temp = count_points(points, center, rad)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_data.append(n)
    time_data.append(average)

plt.plot(n_data, time_data, 'o')
plt.plot(n_data, time_data, 'b')
plt.xlabel('кол-во точек')
plt.ylabel('время выполнения, с.')

plt.show()
