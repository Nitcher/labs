import math


try:
    num = int(input('Выберите функцию 1-G, 2-F или 3-Y:'))
    if num in [1, 2, 3]:
        try:
            a = float(input('Введите a '))
            x = float(input('Введите x '))
        except ValueError:
            print('Переменные введены не правильно')
            exit(1)
    else:
        print('Выберите только между 1, 2 или 3')
except ValueError:
    print('Функция должна быть целым числом')

try:
    if num == 1:
        try:
            G = (- 2 * (7 * a**2 - 17 * a * x + 6 * x**2))/(21 * a**2 + a * x - 36 * x**2)
            print('G=' + str(G))
        except ZeroDivisionError:
            print('Фунция G не имеет решения т.к деление на 0 невозможно.')

    elif num == 2:
        try:
            F = (math.sin(math.pi * (-8 * a**2 + 6 * a * x + 9 * x**2)))/(math.pi * (-8 * a**2 + 6 * a * x + 9 * x**2))
            print('F=' + str(F))
        except ZeroDivisionError:
            print('Функция F  не имеет решения т.к деление на 0 невозможно')

    elif num == 3:
        try:
            Y = (-math.atanh(8 * a**2 - 27 * a * x - 20 * x**2))
            print('Y=' + str(Y))
        except ValueError:
            print('Функция Y не имеет решения т.к переменные не входят в ОДЗ')
except NameError:
    print('')