import math


while True:

    g_lst = []
    f_lst = []
    y_lst = []

    try:
        data = list(map(float, input('Введите минимум, максимум, а, размер шагов через пробел: ').split()))
        x_min = float(data[0])
        x_max = float(data[1])
        a = float(data[2])
        step = float(data[3])
    except ValueError:
        print('Неверный ввод')
        exit(1)

    while x_min < x_max:
        try:
            g = (- 2 * (7 * a ** 2 - 17 * a * x_min + 6 * x_min ** 2)) / (21 * a ** 2 + a * x_min - 36 * x_min ** 2)
            g_lst.append(g)
            f = (math.sin(math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))) / (math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))
            f_lst.append(f)
            y = (-math.atanh(8 * a ** 2 - 27 * a * x_min - 20 * x_min ** 2))
            y_lst.append(y)
        except ZeroDivisionError:
            g = None
            g_lst.append(g)
            f = None
            f_lst.append(f)
        except ValueError:
            y = None
            y_lst.append(y)
        x_min += step

    file = open('lab 7.txt', 'w')
    file.write(f'g: {g_lst}\nf: {f_lst}\ny: {y_lst}')
    file.close()

    g_lst = []
    f_lst = []
    y_lst = []
    temp = []

    file = open('lab 7.txt', 'r')
    for line in file:
        temp.append(line.rstrip().split())
    file.close()

    g_lst = temp[0]
    f_lst = temp[1]
    y_lst = temp[2]
    del temp

    for i in range(len(g_lst)):
        print(f'g = {g_lst[i]}')
    for i in range(len(f_lst)):
        print(f'f = {f_lst[i]}')
    for i in range(len(y_lst)):
        print(f'y = {y_lst[i]}')

    check = input('Вы хотите выйти? Введите yes или no: ')
    if check == 'yes':
        break
    elif check == 'no':
        pass