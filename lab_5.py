import math


while True:
    try:
        num = int(input('Выберите функцию 1-G, 2-F или 3-Y: '))
        if num in [1, 2, 3]:
            x_max = float(input('Введите верхнюю границу x: '))
            x_min = float(input('Введите нижнюю границу x: '))
            a = float(input('Введите а: '))
            step = float(input('Введите размер шага: '))
            template = float(input("Введите шаблон: "))
        else:
            print('Выберите только между 1, 2 или 3')
            exit(1)
    except ValueError:
        print("Функция должна быть целым числом")
        exit(1)
    x_lst = []
    y_lst = []

    while x_min < x_max:
        x_lst.append(x_min)
        if num == 1:
            try:
                G = (- 2 * (7 * a ** 2 - 17 * a * x_min + 6 * x_min ** 2)) / (21 * a ** 2 + a * x_min - 36 * x_min ** 2)
                y_lst.append(G)
                print(f'x = {x_min}, G = {G}')
            except ZeroDivisionError:
                y_lst.append(None)
                print('Фунция G не имеет решения т.к деление на 0 невозможно.')
            x_min += step

        if num == 2:
            try:
                F = (math.sin(math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))) / (math.pi * (-8 * a ** 2 + 6 * a * x_min + 9 * x_min ** 2))
                y_lst.append(F)
                print(f'x = {x_min}, F = {F}')
            except ZeroDivisionError:
                y_lst.append(None)
                print('Фунция F не имеет решения т.к деление на 0 невозможно.')
            x_min += step

        if num == 3:
            try:
                Y = (-math.atanh(8 * a ** 2 - 27 * a * x_min - 20 * x_min ** 2))
                y_lst.append(Y)
                print(f'x = {x_min}, Y = {Y}')
            except ValueError:

                y_lst.append(None)
                print('Функция Y не имеет решения т.к переменные не входят в ОДЗ')
            x_min += step

    our_string = 'f(x) ='
    for element in y_lst:
        our_string +=str(element)
    print(our_string)

    y_lst = 'f(x) = ' + ', '.join([str(x) for x in y_lst])
    resy = y_lst.count(str(template))
    print(f'Шаблон встречается = {resy} раз')

    check = input('Вы хотите выйти? Введите yes или no: ')
    if check == 'yes':
        break
    elif check == 'no':
        pass