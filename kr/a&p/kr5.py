import time
import matplotlib.pyplot as plt
import random
import sys
import numpy as np

n_min = 20
n_max = 200
n_step = 20
counter = 8
size_data = []
time_data = []
n_data = []

def gauss(coef_matr, res_matr):
    def row_divider(matr1, matr2, row, divider):
        matr1[row] = [a / divider for a in matr1[row]]
        matr2[row] /= divider

    def row_comb(mat1, mat2, row, source_row, weight):
        mat1[row] = [(a + k * weight) for a, k in zip(mat1[row], mat1[source_row])]
        mat2[row] += mat2[source_row] * weight

    col = 0
    while col < len(res_matr):
        current_row = None
        for r in range(col, len(coef_matr)):
            if current_row is None or abs(coef_matr[r][col]) > abs(coef_matr[current_row][col]):
                current_row = r

        if current_row is None:
            return None

        if current_row != col:
            coef_matr[current_row], coef_matr[col] = coef_matr[col], coef_matr[current_row]
            res_matr[current_row], res_matr[col] = res_matr[col], res_matr[current_row]

        row_divider(coef_matr, res_matr, col, coef_matr[col][col])

        for r in range(col + 1, len(coef_matr)):
            row_comb(coef_matr, res_matr, r, col, -coef_matr[r][col])
        col += 1

    output = [0 for b in res_matr]
    for i in range(len(res_matr) - 1, -1, -1):
        output[i] = res_matr[i] - sum(x * a for x, a in zip(output[(i + 1):], coef_matr[i][(i + 1):]))

    return output

for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[random.uniform(-10, 10) for j in range(n)] for i in range(n)]
    tmp_mat2 = [random.uniform(-10, 10) for i in range(n)]
    counter += 1
    averages_time = []
    averages_size = []
    for _ in range(3):
        start = time.time()
        averages_size.append(sys.getsizeof(gauss(tmp_mat1, tmp_mat2)))
        averages_time.append(time.time() - start)
    time_data.append(sum(averages_time) / 3)
    size_data.append(sum(averages_size) / 3)
    print(time_data[-1])
    n_data.append(n)

fig = plt.figure()
plt.plot(n_data, time_data, 'o')
trend1 = np.poly1d(np.polyfit(n_data, time_data, 3))
plt.plot(n_data, trend1(n_data), 'b', c='red')
plt.xlabel('размерность матриц')
plt.ylabel('время выполнения')

plt.show()

fig = plt.figure()
plt.plot(n_data, size_data, 'o')
trend2 = np.poly1d(np.polyfit(n_data, size_data, 3))
plt.plot(n_data, trend2(n_data), 'b', c='blue')
plt.xlabel('размерность матриц')
plt.ylabel('объем памяти')

plt.show()