import time
import matplotlib.pyplot as plt


def matrix(a, b):
    return [[sum(x * y for x, y in zip(a_row, b_column)) for b_column in zip(*b)] for a_row in a]


n_min = 10e2
n_max = 10e4
n_step = 10e3
a_float = 10.05
a_int = 10

counter = 6
time_int_data = []
time_float_data = []
n_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[j for j in range(counter)] for i in range(n)]
    tmp_mat2 = zip(*tmp_mat1)
    counter += 1
    averages = []
    for _ in range(3):
        start = time.time()
        temp1 = matrix(tmp_mat1, tmp_mat2)
        averages.append(time.time() - start)
    time_int_data.append(sum(averages) / 3)
    print(time_int_data[-1])
    n_data.append(n)

for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[j for j in range(counter)] for i in range(n)]
    tmp_mat2 = zip(*tmp_mat1)
    counter += 1
    averages = []
    for _ in range(3):
        start = time.time()
        temp1 = matrix(tmp_mat1, tmp_mat2)
        averages.append(time.time() - start)
    time_float_data.append(sum(averages)/3)
    print(time_float_data[-1])

plt.plot(n_data, time_int_data, 'o')
plt.plot(n_data, time_int_data, label = 'int')
plt.legend()
plt.plot(n_data, time_float_data, 'o')
plt.plot(n_data, time_float_data, label = 'float')
plt.legend()
plt.xlabel('длина матриц')
plt.ylabel('время выполнения, с.')

plt.show()

