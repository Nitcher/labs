import sys
import time

try:
    lines = []
    for line in sys.stdin:
        lines.append(line)

    gif = []
    count = 0

    print("\033c", end="")

    try:
        while True:
            if lines[count].count("```") == 1:
                gif.append("".join(["\033[36m" + lines[i] for i in range(count + 1, count + 24)]))
                count += 25
            elif lines[count].count("```") != 1:
                count += 1
    except IndexError:
        pass

    del lines

    while True:
        for line in gif:
            time.sleep(0.5)
            print("\033c", end="")
            print(line)
except KeyboardInterrupt:
    print('Программа завершена')
