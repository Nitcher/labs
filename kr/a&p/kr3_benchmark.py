import time
import matplotlib.pyplot as plt
import random


def vectors(vector_x, vector_y, scalar_a):
    return [vector_x_coord * scalar_a + vector_y_coord for vector_x_coord, vector_y_coord in zip(vector_x, vector_y)]


a_float = 10.5
a_int = 10
n_min = 10e5
n_max = 10e6
n_step = 10e5


n_int_data = []
n_float_data = []
time_int_data = []
time_float_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    temp = [random.uniform(-10, 10) for i in range(n)]
    for _ in range(3):
        start = time.time()
        vectors(temp, temp, a_float)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_float_data.append(n)
    time_float_data.append(average)

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    temp = [random.randint(-10, 10) for i in range(n)]
    for _ in range(3):
        start = time.time()
        vectors(temp, temp, a_int)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_int_data.append(n)
    time_int_data.append(average)


plt.plot(n_float_data, time_float_data, 'o')
plt.plot(n_float_data, time_float_data, 'b')

plt.plot(n_int_data, time_int_data, 'o')
plt.plot(n_int_data, time_int_data, 'b')

plt.xlabel('длина векторов')
plt.ylabel('время выполнения, с.')

plt.show()