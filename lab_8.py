import random
import matplotlib.pyplot as plt
import matplotlib.patches as pch


def point_check(point, center, rad):
    return (center[0] - point[0]) ** 2 + (center[1] - point[1]) ** 2 <= rad ** 2


def count_points(points, center, radius):
    output = 0
    for point in points:
        if point_check(point, center, radius):
            output += 1
    return output


num = int(input('Введите количество точек: '))
rad = float(input('Введите радиус: '))

coords = []

for i in range(num):
    coords.append((random.uniform(-20, 20), random.uniform(-20, 20)))

center = (random.uniform(0, 20), random.uniform(0, 20))
print(f'Найдено точек: {count_points(coords, center, rad)}')

ax = plt.subplot()
ax.scatter(center[0], center[1], c='red', s=11, zorder=100, marker='X')
ax.scatter([x for x, y in coords if point_check((x, y), center, rad) is True],
           [y for x, y in coords if point_check((x, y), center, rad) is True],
           s=9,
           c='green',
           zorder=50,
           marker='p')
ax.scatter([x for x, y in coords if point_check((x, y), center, rad) is False],
           [y for x, y in coords if point_check((x, y), center, rad) is False],
           s=3,
           c='black',
           zorder=0)

ax.add_patch(pch.Circle(center, radius=rad, fill=False))
ax.axis('square')
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)
plt.show()

